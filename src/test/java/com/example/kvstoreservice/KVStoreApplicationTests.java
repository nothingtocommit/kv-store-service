package com.example.kvstoreservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Assertions;

@SpringBootTest
class KVStoreApplicationTests {

	@Autowired
	MappingService mappingService;

	MappingModel mappingWithCustomTtl = new MappingModel(1L, "k", "v", 0L);

	@Test
	public void ttlExpired_mappingRemoved() throws Exception {
		synchronized(mappingService) {
			mappingService.save(mappingWithCustomTtl);
		}
		Assertions.assertNotNull(mappingService.findByKey(mappingWithCustomTtl.getKey()));

		Thread expiredMappingRemover = new Thread(new ExpiredMappingRemover(mappingService));
		expiredMappingRemover.start();
		expiredMappingRemover.join();

		Assertions.assertNull(mappingService.findByKey(mappingWithCustomTtl.getKey()));
	}

}
