package com.example.kvstoreservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MappingControllerFullContextTests {
    @Autowired
    MappingService mappingService;
    @Autowired
    MockMvc mockMvc;

    @Test
    public void loadState_noContentAndStateUpdated() throws Exception {
        try (PrintWriter writer = new PrintWriter("mappings.csv")) {
            String fileContent = "key" +
                    "," +
                    "value" +
                    "\n" +
                    "123" +
                    "," +
                    "321" +
                    "\n";
            writer.write(fileContent);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        mockMvc.perform(
                        get("/mappings/load")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNoContent());

        Assertions.assertFalse(mappingService.findAll().isEmpty());
    }

    @Test
    public void dumpState_noContentAndDumpFileExists() throws Exception {
        mockMvc.perform(
                        get("/mappings/dump")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNoContent());
        Assertions.assertTrue(new File("mappings.csv").exists());
    }
}
