package com.example.kvstoreservice;

import java.io.File;
import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Optional;

@WebMvcTest
@AutoConfigureMockMvc
public class MappingControllerWebLayerTests {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    MappingService mockedService;
    @Autowired
    ObjectMapper mapper;

    MappingModel defaultMapping = new MappingModel( 1L, "key", "value");
    MappingModel updatedMapping = new MappingModel(1L, "key", "diffvalue");

    @Test
    public void getMappingByKey_success() throws Exception {
        Mockito.when(mockedService.findByKey(defaultMapping.getKey()))
                .thenReturn(defaultMapping);

        mockMvc.perform(
                    get("/mappings/" + defaultMapping.getKey())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(defaultMapping.getValue())));
    }

    @Test
    public void getMappingByKey_notFound() throws Exception {
        mockMvc.perform(
                    get("/mappings/" + defaultMapping.getKey())
                    .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.result", is("Mapping is not found")));
    }

    @Test
    public void createMapping_success() throws Exception {
        Mockito.when(mockedService.save(defaultMapping)).thenReturn(defaultMapping);

        mockMvc.perform(
                    post("/mappings")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(defaultMapping))
                )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.result", is("Success")));
    }

    @Test
    public void updateMapping_success() throws Exception {
        Mockito.when(mockedService.findByKey(updatedMapping.getKey())).thenReturn(defaultMapping);
        Mockito.when(mockedService.save(updatedMapping)).thenReturn(updatedMapping);

        mockMvc.perform(
                    post("/mappings")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(updatedMapping))
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is("Success")));
    }

    @Test
    public void saveMapping_validationNotPassed() throws Exception {
        Map<String, String> notValidMapping = new HashMap<>();
        notValidMapping.put("value", "value");
        mockMvc.perform(
                    post("/mappings")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(notValidMapping))
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.result", is("Validation is not passed")));
    }

    @Test
    public void deleteMapping_success() throws Exception {
        Mockito.when(mockedService.deleteByKey(defaultMapping.getKey()))
                .thenReturn(defaultMapping);
        mockMvc.perform(
                        delete("/mappings/" + defaultMapping.getKey())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(defaultMapping.getValue())));
    }

    @Test
    public void deleteMapping_notFound() throws Exception {
        mockMvc.perform(
                        delete("/mappings/" + defaultMapping.getKey())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.result", is("Nothing to delete")));
    }

}
