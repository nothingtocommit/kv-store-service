package com.example.kvstoreservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import java.util.*;

public class ExpiredMappingRemover implements Runnable {
    private final MappingService mappingService;

    ExpiredMappingRemover(MappingService mappingService) {
        this.mappingService = mappingService;
    }

    @Override
    public synchronized void run() {
        List<MappingModel> mappings = new ArrayList<>(mappingService.findAll());
        ListIterator<MappingModel> iterator = mappings.listIterator();

        while (iterator.hasNext()) {
            int curIndex = iterator.nextIndex();
            if (iterator.next().isMappingExpired()) {
                MappingModel mapping = mappings.get(curIndex);

                synchronized(mappingService) {
                    mappingService.deleteById(mapping.getId());
                }
            };
        }
    }
}
