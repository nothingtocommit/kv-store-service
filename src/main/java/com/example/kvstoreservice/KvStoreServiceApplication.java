package com.example.kvstoreservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class KvStoreServiceApplication {
	@Autowired
	MappingService mappingService;

	public static void main(String[] args) {
		SpringApplication.run(KvStoreServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner startCleanupThread() {
		return  new CommandLineRunner() {
			@Override
			public void run(String... args) throws Exception {
				ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
				executorService.scheduleAtFixedRate(new ExpiredMappingRemover(mappingService), 0, 1, TimeUnit.SECONDS);
			}
		};
	}

}
