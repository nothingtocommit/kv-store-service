package com.example.kvstoreservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MappingService {
    @Autowired
    MappingRepo mappingRepo;

    public MappingModel save(MappingModel mapping) {
        return mappingRepo.save(mapping);
    }

    public MappingModel findByKey(String key) {
        return mappingRepo.findByKey(key);
    }

    public List<MappingModel> findAll() {
        List<MappingModel> mappings = new ArrayList<>();
        mappingRepo.findAll().forEach(mappings::add);
        return mappings;
    }

    public MappingModel deleteByKey(String key) {
        return mappingRepo.deleteByKey(key);
    }

    public void deleteById(Long id) {
        mappingRepo.deleteById(id);
    }

}
