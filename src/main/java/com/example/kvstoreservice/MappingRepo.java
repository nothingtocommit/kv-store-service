package com.example.kvstoreservice;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MappingRepo extends CrudRepository<MappingModel, Long> {
    MappingModel findByKey(String key);
    MappingModel deleteByKey(String key);
}
