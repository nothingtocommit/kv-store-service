package com.example.kvstoreservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.*;
import java.util.*;


@RestController
public class MappingController {
    @Autowired
    MappingService mappingService;

//    @GetMapping("/mappings")
//    public List<MappingModel> getMappings() {
//        return mappingService.findAll();
//    }

    @GetMapping("/mappings/{key}")
    public HttpEntity<Object> getMapping(@PathVariable String key) {
        HttpStatus httpStatus;
        Map<String, Object> responseBody = new HashMap<>();
        MappingModel mapping = mappingService.findByKey(key);
        if (mapping == null) {
            responseBody.put("result", "Mapping is not found");
            httpStatus = HttpStatus.NOT_FOUND;
        } else {
            responseBody.put("result", mapping.getValue());
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(responseBody, httpStatus);
    }

    @PostMapping("/mappings")
    public HttpEntity<Object> setMapping(@RequestBody @Valid MappingModel mapping) {
        Map<String, Object> responseBody = new HashMap<>();
        HttpStatus httpStatus;
        MappingModel existingMapping = mappingService.findByKey(mapping.getKey());
        if (existingMapping == null) {
            httpStatus = HttpStatus.CREATED;
        } else {
            mapping.setId(existingMapping.getId());
            httpStatus = HttpStatus.OK;
        }
        mappingService.save(mapping);
        responseBody.put("result", "Success");
        return new ResponseEntity<>(responseBody, httpStatus);
    }

    @DeleteMapping("/mappings/{key}")
    public HttpEntity<Object> deleteMapping(@PathVariable String key) {
        HttpStatus httpStatus;
        Map<String, Object> responseBody = new HashMap<>();
        MappingModel deletedMapping = mappingService.deleteByKey(key);
        if (deletedMapping == null) {
            responseBody.put("result", "Nothing to delete");
            httpStatus = HttpStatus.NOT_FOUND;
        } else {
            responseBody.put("result", deletedMapping.getValue());
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(responseBody, httpStatus);
    }

    @GetMapping("mappings/dump")
    public HttpEntity<Object> dataDump() {
        return jdbcQuery("call csvwrite('mappings.csv', 'select key, value from mappings')");
    }

    @GetMapping("mappings/load")
    public HttpEntity<Object> loadData() {
        return jdbcQuery("merge into mappings(key, value, ttl) key(key) select key, value, extract (epoch from current_timestamp()) + 30 from csvread('mappings.csv')");
    }

    public ResponseEntity<Object> jdbcQuery(String query) {
        HttpStatus httpStatus;
        try {
            Class.forName("org.h2.Driver");
            Connection connection = DriverManager.getConnection("jdbc:h2:mem:kv-store", "sa", "");
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);

            statement.close();
            connection.close();
            httpStatus = HttpStatus.NO_CONTENT;
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(httpStatus);
    }
}
