package com.example.kvstoreservice;

import lombok.Builder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="mappings")
@Builder
public class MappingModel {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotBlank
    private String key;
    @NotBlank
    private String value;

    @Column(columnDefinition = "bigint default extract (epoch from current_timestamp()) + 30")
    private Long ttl = System.currentTimeMillis() / 1000L + 30L;

    MappingModel(Long id, String key, String value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    MappingModel(Long id, String key, String value, Long ttl) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.ttl = ttl;
    }

    MappingModel () {}

    public boolean isMappingExpired() {
        return System.currentTimeMillis() / 1000L > ttl;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public Long getTtl() {
        return ttl;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public void setTtl(Long ttl) {
        this.ttl = ttl;
    }
}
