package com.example.kvstoreservice;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.*;

@ControllerAdvice
public class ExceptionHandlers {
    @ExceptionHandler
    public HttpEntity<Object> handle(MethodArgumentNotValidException ex) {
        Map<String, String> responseBody = new HashMap<>();
        responseBody.put("result", "Validation is not passed");

        return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
    }
}
